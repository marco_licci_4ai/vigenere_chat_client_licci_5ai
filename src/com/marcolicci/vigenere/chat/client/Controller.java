package com.marcolicci.vigenere.chat.client;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

import java.util.function.UnaryOperator;

public class Controller {
    @FXML
    private TextField addressField;
    @FXML
    private TextField portField;
    @FXML
    private TextField nicknameField;
    @FXML
    private TextField vigenereField;
    @FXML
    private TextField inputField;
    @FXML
    private TextArea chatField;


    public Controller() {
        Main.setController(this);
    }

    @FXML
    private void connect() {
        Main.setServerConnection(new ServerConnection(addressField.getText(),
                        Integer.parseInt(portField.getText()),
                        nicknameField.getText(), vigenereField.getText()));
    }

    @FXML
    private void send() {
        Main.getServerConnection().sendMessage(inputField.getText());
        inputField.clear();
    }

    @FXML
    public void initialize() {
        Main.setController(this);
        /*portField.setTextFormatter(
                new TextFormatter<Integer>(
                        new IntegerStringConverter(), 0, integerFilter));*/
    }

    UnaryOperator<TextFormatter.Change> integerFilter = change -> {
        String newText = change.getControlNewText();
        if (newText.matches("-?([1-9][0-9]*)?")) {
            return change;
        }
        return null;
    };

    public void addMessage(String message){
        chatField.appendText(message + "\n");
        chatField.setScrollTop(Float.MAX_VALUE);
    }
}