package com.marcolicci.vigenere.chat.client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

public class ServerConnection {
    private Socket socket;
    private final String address;
    private final int port;
    private final LinkedBlockingQueue<String> outgoingMessages;
    private PrintWriter socketWriter;
    private BufferedReader socketReader;
    private final String nickname;
    private byte[] keyOffsets;
    private Thread incomingHandler;
    private Thread outgoingaHandler;

    public ServerConnection(String address, int port, String nickname, String stringKey) {
        this.address = address;
        this.port = port;
        this.nickname = nickname;
        setKey(stringKey);
        outgoingMessages = new LinkedBlockingQueue<>();
    }

    void init() throws IOException {
        try {
            socket = new Socket(InetAddress.getByName(address), port);
        } catch (IOException e){
            throw e;
        }
        socketWriter = new PrintWriter(socket.getOutputStream(), true);
        socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        handleIncoming();
        handleOutgoing();
    }

    private void handleIncoming(){
        incomingHandler = new Thread(()->{
            String message;
            try {
                while ((message = socketReader.readLine()) != null) {
                    Main.getController().addMessage(decrypt(message));
                }
                close();
            } catch (IOException e) {
                if(e.getMessage().equals("Socket closed")){
                    System.out.println("[incomingHandler] Socket closed, closing.");
                } else {
                    e.printStackTrace();
                }
            }
        });
        incomingHandler.start();
    }

    private void handleOutgoing(){
        outgoingaHandler = new Thread(()->{
            try {
                while (true) {
                    socketWriter.println(encrypt(outgoingMessages.take()));
                }
            } catch (InterruptedException e) {
                System.out.println("[outgoingHandler] Interrupted, closing.");
            }
        });
        outgoingaHandler.start();
    }

    void sendMessage(String message){
        outgoingMessages.offer("(" + nickname + ") " + message);
    }

    private char[] encrypt(String text){
        char[] textChars = text.toCharArray();
        if(keyOffsets.length != 0) {
            for (int i = 0; i < textChars.length; i++) {
                textChars[i] += keyOffsets[i % keyOffsets.length];
            }
        }
        return textChars;
    }

    private String decrypt(String text){
        char[] textChars = text.toCharArray();
        if(keyOffsets.length != 0) {
            for (int i = 0; i < textChars.length; i++) {
                textChars[i] -= keyOffsets[i % keyOffsets.length];
            }
        }
        return new String(textChars);
    }

    private void setKey(String stringKey){ //todo verify key validity
        keyOffsets = stringKey.getBytes();
        for (int i = 0; i < keyOffsets.length; i++) {
            keyOffsets[i] -= 65;
        }
    }

    public void close(){
        try {
            socket.close();
            incomingHandler.interrupt();
            outgoingaHandler.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
