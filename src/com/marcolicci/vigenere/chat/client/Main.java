package com.marcolicci.vigenere.chat.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    private static ServerConnection serverConnection;
    private static Controller controller;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("view.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 1000, 500));
        primaryStage.show();
        primaryStage.setOnCloseRequest((windowEvent)->{
            if (serverConnection != null) {
                serverConnection.close();
            }
        });
    }


    public static void main(String[] args) {
        launch(args);
    }

    public static ServerConnection getServerConnection() {
        return serverConnection;
    }

    public static void setServerConnection(ServerConnection serverConnection) {
        if(Main.serverConnection != null){
            Main.serverConnection.close();
        }

        try{
            serverConnection.init();
            Main.serverConnection = serverConnection;
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public static Controller getController() {
        return controller;
    }

    public static void setController(Controller controller) {
        Main.controller = controller;
    }
}
